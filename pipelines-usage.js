'use strict';

var request = require('request')

var debug = false
var days = 30

var username = process.argv[2]
var repo_slugs = process.argv.slice(3)
var password = process.env.BB_PASSWORD
if (!username || !repo_slugs || !password) {
    console.warn(`Usage: ${process.argv[1]} username repo...`)
    process.exit(1)
}
if (!password) {
    console.warn("BB_PASSWORD should be set as an environment variable")
    process.exit(1)
}

function daysAgo(time) {
    var secondsAgo = new Date().getTime() - time
    return Math.floor(secondsAgo / 1000 / 60 / 60 / 24)
}

function summarise(build) {
    return {
        build_number: build.build_number,
        build_seconds_used: build.build_seconds_used,
        days_ago: daysAgo(new Date(build.created_on).getTime()),
        created_on: build.created_on
    }
}

function getPipelines(repo_slug, page) {
    const repo = repo_slug.includes("/") ? repo_slug : `${username}/${repo_slug}`
    const pipelines_url = `https://api.bitbucket.org/2.0/repositories/${repo}/pipelines/`
    const params = {
        url: pipelines_url,
        qs: {
            sort: "-created_on",
            page: page,
            pagelen: 100
        },
        auth: {
            user: username,
            pass: password
        },
        json: true
    }
    return new Promise((resolve, reject) => {
        if (debug) console.log(`Retrieving ${params.url} with params: ${JSON.stringify(params.qs)}`)
        request.get(params, (error, response, body) => {
            if (error || response.statusCode != 200) {
                reject(`Got status ${response.statusCode}, error "${error}" while retrieving "${pipelines_url}"`)
            } else {
                resolve([ page, body.values.map(summarise) ])
            }
        })
    })
}

function log(summary) {
    console.log(summary.build_number + "\t" + summary.days_ago)
    return summary
}

function isNewerThan(days) {
    return (summary) => { return summary.days_ago <= days }
}

function appendTo(array) {
    return (summary) => { array.push(summary); return summary }
}

function getRecentBuilds(repo_slug, page, builds) {
    return getPipelines(repo_slug, page).then(([ page, summaries ]) => {
        // append builds newer than X days to the results
        summaries.filter(isNewerThan(days))
            .map(appendTo(builds))

        // if all results are new, keep searching
        if (summaries.length > 0 && summaries.every(isNewerThan(days))) {
            return getRecentBuilds(repo_slug, page + 1, builds)
        }
    })
}

function formatSeconds(seconds) {
    var result = ""
    var hrs = Math.floor(seconds / 3600)
    if (hrs > 0) {
        result += hrs + "h "
        seconds -= hrs * 3600
    }
    var [mins, secs] = [Math.floor(seconds / 60), seconds % 60]
    result += `${mins}m ${secs}s`
    return result
}

console.log(`Showing builds in the last ${days} days`)
console.log(["Repository", "Builds", "Build minutes"].join("\t"))

repo_slugs.forEach(function (repo_slug) {
    var builds = [];
    getRecentBuilds(repo_slug, 1, builds).then(() => {
        var total_secs = builds.reduce((sum, build) => {
            return sum + build.build_seconds_used
        }, 0)
        console.log([repo_slug, builds.length, formatSeconds(total_secs)].join("\t"))
    }).catch((message) => {
        console.warn(message)
    })
})
